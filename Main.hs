import Util
import Data.Maybe
import Data.List
import Test.HUnit

-- Ejemplos
singletonUno = singleton 1
singletonA = singleton 'a'
singletonHelloWorld = singleton "HelloWorld"

anilloEjemplo2::Anillo Integer
anilloEjemplo2 = A 5 proximo
  where proximo n | n == 5 = Just 8
                  | n == 8 = Just 3
                  | n == 3 = Just 7
                  | n == 7 = Just 5
                  | n == 9 = Just 9
                  | otherwise = Nothing

-- Ejercicio 1
singleton:: Eq t => t -> Anillo t
singleton t = A t (\p -> if p==t then Just p else Nothing)

insertar :: Eq t => t -> Anillo t -> Anillo t
insertar t (A e f) = A e (\p -> if p==e then Just t else if p==t then f e else f p)

avanzar :: Anillo t -> Anillo t
avanzar a = A (fromJust (siguiente a (actual a))) (siguiente a)

-- Ejercicio 2
enAnillo:: Eq t => t -> Anillo t -> Bool
enAnillo t a =
  elem
    (Just t)
    ((Just (actual a)):
      takeWhile
        (\x -> x /= Just (actual a))
        (iterate ((siguiente a).fromJust) (Just (actual (avanzar a)))))

-- Ejercicio 3
filterAnillo :: Eq t => (t -> Bool) -> Anillo t -> Maybe (Anillo t)
filterAnillo g a =
  foldr
    (\x rec ->
      if isNothing rec
      then Just (singleton (fromJust x))
      else Just (insertar (fromJust x) (fromJust rec)))
    Nothing
    (filter
      (g.fromJust)
      ((Just (actual a)):
        takeWhile
          (\x -> (siguiente a) (fromJust x) /= Just (actual a))
          (iterate ((siguiente a).fromJust) (Just (actual a)))))

-- Ejercicio 4
mapAnillo:: Eq a => Eq b => (a -> b) -> Anillo a -> Anillo b
mapAnillo g a =
  foldr
    (\x rec -> insertar (g (fromJust x)) rec)
    (singleton (g (actual a)))
    (takeWhile
      (\x -> (fromJust x) /= (actual a))
      (iterate ((siguiente a).fromJust) (Just (actual (avanzar a)))))

--Ejercicio 5
palabraFormable :: String -> [Anillo Char] -> Bool
palabraFormable str xs = foldr (\x rec -> enAnillo (fst x) (snd x) && rec) True (zip str xs)

--Ejercicio 6
listaAAnillo :: Eq a => [a] -> Anillo a
listaAAnillo xs = foldr (\x rec -> insertar x rec) (singleton (last xs)) xs

intercalaciones :: a -> [a] -> [[a]]
intercalaciones t xs = [(take i xs)++[t]++(drop i xs) | i <-[0 .. length xs]]

permutaciones :: [a] -> [[a]]
permutaciones = foldr (\t rec -> concatMap (intercalaciones t) rec) [[]]

partes :: [a] -> [[a]]
partes xs = foldr (\x rec -> [[x]] ++ map (x:) rec ++ rec) [] xs

anillos:: Eq a => [a] -> [Anillo a]
anillos xs = map (listaAAnillo) (concatMap permutaciones (partes xs))

--Ejecución de los tests
main :: IO Counts
main = do runTestTT allTests

allTests = test [
  "ejercicioSingleton" ~: testsSingleton,
  "ejercicioInsertar" ~: testsInsertar,
  "ejercicioAvanzar" ~: testsAvanzar,
  "ejercicioEnAnillo" ~: testsEnAnillo,
  "ejercicioFilterAnillo" ~: testsFilterAnillo,
  "ejercicioMapAnillo" ~: testsMapAnillo,
  "ejercicioPalabraFormable" ~: testsPalabraFormable,
  "ejercicioAnillos" ~: testsAnillos
  ]

testsSingleton = test [
  1 ~=? actual singletonUno,
  'a' ~=? actual singletonA,
  "HelloWorld" ~=? actual singletonHelloWorld,
  actual singletonUno ~=? fromJust (siguiente singletonUno 1),
  actual singletonA ~=? fromJust (siguiente singletonA 'a'),
  actual singletonHelloWorld ~=? fromJust (siguiente singletonHelloWorld "HelloWorld"),
  Nothing ~=? siguiente singletonUno 2
  ]

testsInsertar = test [
  1 ~=? actual (insertar 2 singletonUno),
  'b' ~=? fromJust (siguiente (insertar 'b' singletonA) 'a'),
  'a' ~=? fromJust (siguiente (insertar 'b' singletonA) 'b'),
  Nothing ~=? siguiente (insertar 'b' singletonA) 'z'
  ]

testsAvanzar = test [
  5 ~=? actual anilloEjemplo,
  8 ~=? actual (avanzar anilloEjemplo),
  siguiente anilloEjemplo 5 ~=? siguiente (avanzar anilloEjemplo) 5,
  siguiente anilloEjemplo 100 ~=? siguiente (avanzar anilloEjemplo) 100
  ]

testsEnAnillo = test [
  True ~=? enAnillo 5 anilloEjemplo,
  True ~=? enAnillo 8 anilloEjemplo,
  True ~=? enAnillo 3 anilloEjemplo,
  True ~=? enAnillo 7 anilloEjemplo,
  True ~=? enAnillo 5 anilloEjemplo2,
  True ~=? enAnillo 8 anilloEjemplo2,
  True ~=? enAnillo 3 anilloEjemplo2,
  True ~=? enAnillo 7 anilloEjemplo2,
  False ~=? enAnillo 9 anilloEjemplo,
  False ~=? enAnillo 9 anilloEjemplo2
  ]

testsFilterAnillo = test [
  True ~=? enAnillo 8 (fromJust (filterAnillo (>5) anilloEjemplo)),
  False ~=? enAnillo 3 (fromJust (filterAnillo (>5) anilloEjemplo)),
  True ~=? isNothing (filterAnillo (>100) anilloEjemplo)
  ]

testsMapAnillo = test [
  True ~=? enAnillo 5 (mapAnillo (`mod` 6) anilloEjemplo),
  True ~=? enAnillo 2 (mapAnillo (`mod` 6) anilloEjemplo),
  True ~=? enAnillo 3 (mapAnillo (`mod` 6) anilloEjemplo),
  True ~=? enAnillo 1 (mapAnillo (`mod` 6) anilloEjemplo),
  15 ~=? actual (mapAnillo (+10) anilloEjemplo),
  10 ~=? actual (mapAnillo (*2) anilloEjemplo)
  ]

anilloA1 = insertar 'l' (insertar 'f' (insertar 'q' (singleton 'a')))
anilloA2 = insertar 'u' (singleton 'x')
anilloA3 = insertar 'a' (insertar 'z' (singleton 'i'))
anilloA4 = insertar 'n' (insertar 's' (insertar 'd' (singleton 'p')))
anillosEjemplo = [anilloA1, anilloA2, anilloA3, anilloA4]

testsPalabraFormable = test [
  True ~=? palabraFormable "luis" anillosEjemplo,
  True ~=? not (palabraFormable "flan" anillosEjemplo),
  True ~=? palabraFormable "quid" anillosEjemplo
  ]

factorial :: Int -> Int
factorial n = product [1..n]

totalPermutaciones :: Int -> Int
totalPermutaciones n = (\n -> sum (map (\k -> div (factorial n) (factorial k)) [0..n-1])) n

testsAnillos = test [
  (totalPermutaciones 2) ~=? length (anillos "ab"),
  (totalPermutaciones 3) ~=? length (anillos "abc"),
  (totalPermutaciones 4) ~=? length (anillos "abcd")
  ]
